ec2-bulk-tagging
Tags all EC2 instances in a region with your tags. Use your own keys and values in the EC2 command.

Command format: aws ec2 create-tags --resources $instanceid --tags Key=Key1,Value=Value1   Key=Key2,Value=Key2 Key=Key3,Value=Value3

Prerequisites

AWS CLI installed and configured.

Install: https://docs.aws.amazon.com/cli/latest/userguide/installing.html
Configure: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-quick-configuration



Usage

sh EC2BulkTagger.sh

OR

chmod 400 EC2BulkTagger.sh
./EC2BulkTagger.sh

### Tags all EC2 instances in a region with your tags ###
### Use your own keys and values in the EC2 command ###
### Command format: aws ec2 create-tags --resources $instanceid --tags Key=Key1,Value=Value1   Key=Key2,Value=Key2 Key=Key3,Value=Value3 ###

#!/bin/bash

#### OPTIONAL ####
#### RUN only FOR CURRENT REGION ####
#instanceid=$(aws ec2 describe-instances | grep InstanceId | cut -d'"' -f4)
#echo processing instances $instanceid
#aws ec2 create-tags --resources $instanceid --tags Key=Key1,Value=Value1   Key=Key2,Value=Key2 Key=Key3,Value=Value3
#echo DONE


#### SPECIFYING REGION ####

echo "Available AWS regions:
us-east-1	US East (N. Virginia)
us-east-2	US East (Ohio)
us-west-1	US West (N. California)
us-west-2	US West (Oregon)
ca-central-1	Canada (Central)
eu-central-1	EU (Frankfurt)
eu-west-1	EU (Ireland)
eu-west-2	EU (London)
eu-west-3	EU (Paris)
ap-northeast-1	Asia Pacific (Tokyo)
ap-northeast-2	Asia Pacific (Seoul)
ap-southeast-1	Asia Pacific (Singapore)
ap-southeast-2	Asia Pacific (Sydney)
ap-south-1	Asia Pacific (Mumbai)
sa-east-1	South America (São Paulo)"

echo Enter region:
read regionname

instanceid=$(aws ec2 --region $regionname describe-instances | grep InstanceId | cut -d'"' -f4)
echo processing instances $instanceid in $regionname

aws ec2 --region $regionname create-tags --resources $instanceid --tags Key=Key1,Value=Value1   Key=Key2,Value=Key2 Key=Key3,Value=Value3

echo DONE
